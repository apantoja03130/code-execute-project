﻿using System;
using System.IO;

namespace CodeExecutor.Utils
{
    public class FileOperations
    {
        public static string SyncReadFile(string fileAddress)
        {
            using (StreamReader reader = new StreamReader(fileAddress))
            {
                return reader.ReadToEnd();
            }
        }
        public static void SyncWriteFile(string fileAddress, string text)
        {
            using (StreamWriter writer = new StreamWriter(fileAddress))
            {
                writer.WriteLine(text);
            }
        }
    }
}
