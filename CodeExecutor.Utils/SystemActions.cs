﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace CodeExecutor.Utils
{
    public class SystemActions
    {
        public static void ExecCommands(string filePath)
        {
            ProcessStartInfo psi = new ProcessStartInfo(@filePath);
//            ProcessStartInfo psi = new ProcessStartInfo(@"C:\\cygwin64\\bin\\bash.exe",filePath);
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            psi.UseShellExecute = false;
            Process p = new Process();
            p.StartInfo = psi;
            p.Start();
            string result = p.StandardOutput.ReadToEnd();
        }
    }
}
