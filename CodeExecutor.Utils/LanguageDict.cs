﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeExecutor.Utils
{
    public class LanguageDict
    {
        static Dictionary<string, string> langDictionary = new Dictionary<string, string>()
        {
            {"C", "c" },
            {"Java", "java" },
            {"C#", "cs" },
            {"Python", "py" },
            {"Javascript", "js" },
            {"Ruby", "rb" },
            {"C++", "cpp" }
        };
        public static string GetExtension(string language) => langDictionary[language];
    }
}
