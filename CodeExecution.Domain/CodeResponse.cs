﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeExecution.Domain
{
    public class CodeResponse
    {
        public OpeResponse Compilation { get; set; }
        public OpeResponse Execution { get; set; }
    }
}
