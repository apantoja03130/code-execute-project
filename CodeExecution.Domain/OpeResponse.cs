﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeExecution.Domain
{
    public class OpeResponse
    {
        public string ExitCode { get; set; }
        public string ErrorFile { get; set; }
        public string OutputFile { get; set; }
    }
}
