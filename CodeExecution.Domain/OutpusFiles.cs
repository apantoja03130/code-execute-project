﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeExecution.Domain
{
    public class OutpusFiles
    {
        public string CompilationOutput { get; set; }
        public string CompilationError { get; set; }
        public string ExecutionOutput { get; set; }
        public string ExecutionError { get; set; }
    }
}
