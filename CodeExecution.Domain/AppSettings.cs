﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeExecution.Domain
{
    public class AppSettings
    {
        public string BasePath { get; set; }
        public string ProgramName { get; set; }
        public Container ContainersPath { get; set; }
        public OutpusFiles ContainersSharedPath { get; set; }
    }
}
