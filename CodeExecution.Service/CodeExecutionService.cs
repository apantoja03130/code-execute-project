﻿using CodeExecution.Domain;
using CodeExecutor.Utils;
using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;
using System.IO;

namespace CodeExecution.Service
{
    public class CodeExecutionService : ICodeExecutionService
    {
        private readonly IConfiguration _config;
        public CodeExecutionService(IConfiguration config)
        {
            _config = config;
        }
        public CodeResponse Execute(CodeInformation codeInformation)
        {
            var codeExecutionSettings = _config.GetSection("CodeExecutionSettings");
            string basePath = codeExecutionSettings.GetSection("BasePath").Value;
            string programName = codeExecutionSettings.GetSection("ProgramName").Value;
            string containerPath = codeExecutionSettings.GetSection("ContainersPath").GetSection(codeInformation.Language).Value;
            string fileExtension = LanguageDict.GetExtension(codeInformation.Language);
            string fileName = programName + '.' + fileExtension;
            string cleanFile = codeExecutionSettings.GetSection("CleanFile").Value;
            string compilationOutput = codeExecutionSettings.GetSection("ContainersSharedPath").GetSection("CompilationOutput").Value;
            string compilationError = codeExecutionSettings.GetSection("ContainersSharedPath").GetSection("CompilationError").Value;
            string compilationExitCode = codeExecutionSettings.GetSection("ContainersSharedPath").GetSection("CompilationExitCode").Value;
            string executionOutput = codeExecutionSettings.GetSection("ContainersSharedPath").GetSection("ExecutionOutput").Value;
            string executionError = codeExecutionSettings.GetSection("ContainersSharedPath").GetSection("ExecutionError").Value;
            string executionExitCode = codeExecutionSettings.GetSection("ContainersSharedPath").GetSection("ExecutionExitCode").Value;
            SystemActions.ExecCommands(cleanFile);
            FileOperations.SyncWriteFile(Path.Combine(basePath, fileName), codeInformation.Program);
            SystemActions.ExecCommands(containerPath);
            OpeResponse compilationResponse = new OpeResponse();
            compilationResponse.OutputFile = FileOperations.SyncReadFile(compilationOutput);
            compilationResponse.ErrorFile = FileOperations.SyncReadFile(compilationError);
            compilationResponse.ExitCode = FileOperations.SyncReadFile(compilationExitCode);
            OpeResponse executionResponse = new OpeResponse();
            executionResponse.OutputFile = FileOperations.SyncReadFile(executionOutput);
            executionResponse.ErrorFile = FileOperations.SyncReadFile(executionError);
            executionResponse.ExitCode = FileOperations.SyncReadFile(executionExitCode);
            CodeResponse codeResponse = new CodeResponse()
            {
                Compilation = compilationResponse,
                Execution = executionResponse
            };
            return codeResponse;
        }
    }
}
