﻿using CodeExecution.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeExecution.Service
{
    public interface ICodeExecutionService
    {
        public CodeResponse Execute(CodeInformation codeInformation);
    }
}
