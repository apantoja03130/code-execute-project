#!bin/bash
mkdir -p /develop/compilation
touch /develop/compilation/log.txt
touch /develop/compilation/error.txt
touch /develop/compilation/exitcode.txt
{ out=$(javac /develop/Main.java); } 2>/develop/compilation/error.txt
#OUT=$(java /develop/Main.java)
EC=$?
if [[ $EC == 0 ]] 
then	 
#	echo $OUT > /develop/results/compile/log.txt
	echo 'Successful compilation!' > /develop/compilation/log.txt
else
	echo 'Error during compilation!' > /develop/compilation/log.txt
#	java /develop/Main.java 2>&1 |tee -a /develop/results/compile/error.txt
#	echo "Fail"
fi
echo $EC > /develop/compilation/exitcode.txt
