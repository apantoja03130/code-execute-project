#!/bin/bash
mkdir -p /develop/compilation
touch /develop/compilation/log.txt 
touch /develop/compilation/error.txt 
touch /develop/compilation/exitcode.txt 
 
{ out=$(gcc -Wall -o /develop/Main /develop/Main.c); } 2>/develop/compilation/error.txt
EC=$?
if [[ $EC == 0 ]] 
then	 
	echo 'Successful compilation!' > /develop/compilation/log.txt
else
	echo 'Error during compilation!' > /develop/compilation/log.txt
fi
echo $EC > /develop/compilation/exitcode.txt