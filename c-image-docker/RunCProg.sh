#!/bin/bash
for F in *.c; do
    { out=$(gcc -Wall -o ${F%.c} $F); } 2>/data/Report_${F%.c}.txt
    if [ $? -ne 0 ]; then
        echo "------------------" >> /data/Report_${F%.c}.txt
        echo $F Errors >> /data/Report_${F%.c}.txt
        echo "" > /data/Result_${F%.c}.txt        
    else
        if grep --quiet "warning:" /data/Report_${F%.c}.txt ; then
            echo "-----------------" >> /data/Report_${F%.c}.txt
            echo $F Warnings >> /data/Report_${F%.c}.txt
        else
            echo "-----------------" >> /data/Report_${F%.c}.txt
            echo $F OK >> /data/Report_${F%.c}.txt
        fi
        ./${F%.c} > /data/Result_${F%.c}.txt
    fi
done